//Requires
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var DBPATH = require('./config/config').DBPATH;

//Require Routers
var usuarioRoutes = require('./routes/usuario');
var loginRoutes = require('./routes/login');
var tareaRoutes = require('./routes/tarea');

//inicializar variables
var app = express();
mongoose.set('useCreateIndex', true);
//BodyParser
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//Mongoose
const mongo = mongoose.connect(DBPATH, { useNewUrlParser: true });
mongo.then(() => {
    console.log('connected');
}).catch((err) => {
    console.log('err', err);
});

//Rutas
app.use('/usuario', usuarioRoutes);
app.use('/login', loginRoutes);
app.use('/tarea', tareaRoutes);

//escuchar peticiones
app.listen(3000, () => {
    console.log('Express funcionando en puerto 3000: \x1b[32m%s\x1b[0m', 'online');
});