var jwt = require('jsonwebtoken');
var SEED = require('../config/config').SEED;


//=================================
//MiddleWares
//=================================
const middlewares = {

    verificaToken: function(req, res, next) {
        var token = req.query.token;
        jwt.verify(token, SEED, (err, decoded) => {
            if (err) {
                return res.status(401).json({
                    ok: true,
                    mensaje: 'Error no esta autorizado',
                    error: err
                });
            }
            //req.usuario = decode.usuario;
            next();
        });
    }
};
module.exports = middlewares;