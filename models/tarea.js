//Require
var mongoose = require('mongoose');
//Mongoose
var Schema = mongoose.Schema;
//Schema
var tareaSchema = new Schema({
    descripciontarea: { type: String, require: [true, 'La tarea es requerida'] },
    estado: { type: Boolean,  require: false },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collections: 'tareas' });
module.exports = mongoose.model('Tarea', tareaSchema);