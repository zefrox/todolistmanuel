//Require
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
//Mongoose
var Schema = mongoose.Schema;
//Schema
var usuarioSchema = new Schema({
    nombre: { type: String, require: [true, 'El nombre es requerido'] },
    nombreusuario: { type: String, unique: true, require: [true, 'El nombre de usuario es requerido'] },
    contrasena: { type: String, require: [true, 'La contraseña es requerido'] }
});
usuarioSchema.plugin(uniqueValidator, { message: '{PATH} debe ser unico' });
module.exports = mongoose.model('Usuario', usuarioSchema);