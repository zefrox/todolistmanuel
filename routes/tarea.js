//Require
var express = require('express');
var Tarea = require('../models/tarea');
var jwt = require('jsonwebtoken');
var mdAutenticacion = require('../middlewares/autenticacion');

//Express
var app = express();

//=================================
//Obtener todas las tareas de un usuario
//=================================
app.get('/', mdAutenticacion.verificaToken, (req, res) => {
    var usuarioConectado = jwt.decode(req.query.token);
    Tarea.find({ usuario: usuarioConectado.usuario._id }, (err, resultado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                error: err
            });
        }
        res.status(200).json({
            ok: true,
            tareas: resultado
        });
    });
});

//=================================
//Insertar una tarea
//=================================

app.post('/insertar', mdAutenticacion.verificaToken, (req, res) => {
    var usuarioConectado = jwt.decode(req.query.token);
    var body = req.body;
    var tarea = new Tarea({
        descripciontarea: body.descripciontarea,
        estado: false,
        usuario: usuarioConectado.usuario._id
    });
    tarea.save((err, tareaDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                error: err,
                mensaje: 'Error al intentar crear la tarea'
            });
        }
        res.status(201).json({
            ok: true,
            mensaje: 'Tarea creada correctamente'
        });
    });
});


//=================================
//Marcar una tarea
//=================================

app.put('/terminar/:id', mdAutenticacion.verificaToken, (req, res) => {
    var id = req.params.id;
    Tarea.findById(id, (err, tareaDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                error: err,
                mensaje: 'Error al intentar terminar tarea'
            });
        }
        if (!tareaDB) {
            return res.status(500).json({
                ok: false,
                mensaje: 'La tarea que se quiere terminar no existe'
            });
        }
        tareaDB.estado = true;
        tareaDB.save((err, tareaTerinada) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    error: err,
                    mensaje: 'Error al intentar terminar Tarea'
                });
            }
            res.status(200).json({
                ok: true,
                mensaje: 'Tarea terminada'
            });
        });
    });
});


//=================================
//Eliminar una tarea
//=================================

app.delete('/eliminar/:id', mdAutenticacion.verificaToken, (req, res) => {
    var id = req.params.id;
    Tarea.findByIdAndRemove(id, (err, tareaEliminada) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                error: err,
                mensaje: 'Error al intentar eliminar la tarea'
            });
        }
        if (!tareaEliminada) {
            return res.status(400).json({
                ok: false,
                mensaje: 'La tarea que se quiere eliminar no existe'
            });
        }
        res.status(200).json({
            ok: true,
            mensaje: 'La tarea fue eliminada'
        });
    });
});
module.exports = app;