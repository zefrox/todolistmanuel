//Require
var express = require('express');
var bcrypt = require('bcrypt');
var Usuario = require('../models/usuario');

//Express
var app = express();
//=================================
//Insertar un usuarios
//=================================

app.post('/insert', (req, res) => {
    var body = req.body;

    var usuario = new Usuario({
        nombre: body.nombre,
        nombreusuario: body.nombreusuario,
        contrasena: bcrypt.hashSync(body.contrasena, 10)
    });
    usuario.save((err, usuarioGuardado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            });
        }
        res.status(201).json({
            ok: true,
            usuarios: usuarioGuardado
        });

    });
});

module.exports = app;