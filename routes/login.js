//Require
var express = require('express');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var SEED = require('../config/config').SEED;
var Usuario = require('../models/usuario');

//Express
var app = express();


//=================================
//Login
//=================================
app.post('/', (req, res) => {
    var body = req.body;
    Usuario.findOne({ nombreusuario: body.nombreusuario }, (err, usuarioDB) => {
        if (err) {
            res.status(500).json({
                ok: false,
                mensaje: 'Error en login',
                error: err
            });
        } else if (!usuarioDB) {
            res.status(400).json({
                ok: false,
                mensaje: 'Error Los datos del login no son validos',
            });
        } else if (!bcrypt.compareSync(body.contrasena, usuarioDB.contrasena)) {
            res.status(400).json({
                ok: false,
                mensaje: 'Error Los datos del login no son validos',
            });
        } else {
            var token = jwt.sign({ usuario: usuarioDB }, SEED, { expiresIn: 14400 }); //4 horas
            res.status(200).json({
                ok: true,
                token: token,
                mensaje: 'login post correcto'
            });
        }
    });

});


module.exports = app;